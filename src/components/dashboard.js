import React, { Component} from 'react';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form'
import StarRatings from 'react-star-ratings';

class Movies extends Component {
  constructor(){
      super();
      this.state = {
        movieInfo: [],
        details: [],
        redirect: false
      };
    }
  setRedirect = () => {
      this.setState({
        redirect: false
      })
  }
  redirectHome() {
    document.querySelector('#show').className = 'd-none';
    // document.querySelector('#hide').className = 'd-inline-flex';
    return window.location.reload(true);
  }
  getData() {
    const header = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json',
        'Authorization': 'Bearer Wookie2019'}
    };
      return fetch('https://wookie.codesubmit.io/movies', header)
    .then(response => response.json())
    .then(data => {
        // console.log(data.movies);
        // this.exportToJson(data.movies);
        this.setState({movieInfo: data.movies})
    })
    .catch(error => console.log(error));
  }
  exportToJson(objectData) {
    let filename = "export.json";
    let contentType = "application/json;charset=utf-8;";
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      var blob = new Blob([decodeURIComponent(encodeURI(JSON.stringify(objectData)))], { type: contentType });
      navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      var a = document.createElement('a');
      a.download = filename;
      a.href = 'data:' + contentType + ',' + encodeURIComponent(JSON.stringify(objectData));
      a.target = '_blank';
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }
  }
  renderCard = (card, index) => {
      return (
            <Card key={index} style={{width: "15rem", height: '100%', marginLeft: '2rem', margin: "1rem"}} onClick={(event) => this.cardClicked(event)}>
                <Card.Img variant="top" style={{ width: "15rem", cursor: "pointer"}} src={card.backdrop} />
            </Card>
            
      );
  }
  convertToYear(date) {
    let dataDate = new Date(date);
    let fullYear = dataDate.getFullYear();
    return JSON.stringify(fullYear);
  }
  detailsCard = (props) => {
    return (
       <Col style={{width: "100rem"}}>
            <Row>
                <Col>
                    <Card style={{width: "30rem", height: '100%', marginLeft: '2rem', margin: "1rem"}}>
                        <Card.Img variant="top" style={{ width: "30rem", height: '100%' }} src={props.backdrop} />
                    </Card>
                </Col>
                <Col>
                    <Row>
                        <Col>
                            <div className="mt-3">
                                <h3>{props.title} <span style={{fontSize: "1rem", fontStyle: "italic"}}>({props.imdb_rating})</span></h3>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <div className="mt-3">
                                <h5>{this.convertToYear(props.released_on)} | {props.length} | {props.director}</h5>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <div className="mt-4 text-justify">
                                {props.overview}
                            </div>
                        </Col>
                    </Row>
                </Col>
                <Col>
                    <div className="text-right">
                        <StarRatings
                        rating={props.imdb_rating}
                        starRatedColor="blue"
                        changeRating={this.changeRating}
                        numberOfStars={10}
                        name='rating'
                        starSpacing="0.02rem"
                        />
                    </div>
                </Col>
            </Row>
       </Col>
    )
  }
  cardClicked(cardsrc) {
    document.querySelector('#hide').className = 'd-none';
    document.querySelector('#show').className = 'd-inline-block';
    let src = cardsrc.target.currentSrc;
    console.log(src);
    return this.state.movieInfo.map((result) => {
        if(result.backdrop === src) {
            console.log(result);
            return this.setState({details: result});
        };
        return result;
    })
  }
  render() {
    this.getData();
    return (
        <Container fluid style={{ marginTop:'1%' }}>
            <Row>
                <Col>
                    <Card>
                        <Card.Body>
                            <Row>
                                <Col style={{ fontSize:'3rem' }}>WOOKIE <br/> MOVIES</Col>
                                <Col>&nbsp;</Col>
                                <Col>
                                    <Row>
                                        <Col>
                                            <Form.Group className="text-right">
                                                <img src="https://github.com/thesatyasahoo/My-codes/blob/master/home-logo-png-icon.png?raw=true" onClick={() => this.redirectHome()}
                                                    style={{width: "3rem", height: "100%", cursor: 'pointer'}} alt="Home" />
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Form.Group>
                                                &nbsp;
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <Form.Group style={{ marginTop:'1rem' }}>
                                                <Form.Control size="sm" type="text" placeholder="Search...." />
                                            </Form.Group>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row style={{ marginLeft:'0.5rem' }} id="hide">
                {this.state.movieInfo.map(this.renderCard)}
            </Row>
            <Row style={{ marginLeft:'0.5rem' }} id="show" className="d-none">
                {this.detailsCard(this.state.details)}
            </Row>
        </Container>
    );
  }
}
 
export default Movies;