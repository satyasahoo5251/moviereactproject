// import logo from './logo.svg';
import React, { Component } from 'react';
import './App.css';
import Movies from './components/movies';

import 'bootstrap/dist/css/bootstrap.min.css';
class App extends Component {
  render() {
    return (
      <div>
        <Movies />
      </div>
    );
  }
}

export default App;
